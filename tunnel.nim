import tables, asyncdispatch, asyncnet, strutils
import types

var connections {.threadvar.}: TableRef[string, Conn]
var tunnels {.threadvar.}: TableRef[string, seq[string]]

proc send(c: Conn, line: string) {.async.} =
  await c.sock.send(line & "\c\L")

proc parseSetup(line: string): tuple[ctype, name: string] =
  var info = line.split(seps = {':'}, maxsplit = 1)
  if info.len < 2:
    result = ("", "")
  else:
    result = (info[0], info[1].replace(':', '-'))

proc echoBack(name, line: string) {.async.} =
  var conn = connections[tunnels[name][0]]
  await conn.send(line)


proc setup(name: string, cttype: ConnType, sock: AsyncSocket) {.async.} =
  connections[name] = Conn(ctType: cttype, sock: sock, label: name)
  await sock.send("setup success" & "\c\L")

proc setup(sock: AsyncSocket): Future[bool] {.async.} =
  let
    line = await sock.recvLine
    (ctype, name) = line.parseSetup
  if name == "" or ctype == "":
    await sock.send("Invalid setup format, disconnect" & "\c\L")
    sock.close
    result = false
  elif name in connections:
    await sock.send(name & " already connected, disconnect" & "\c\L")
    sock.close
    result = false
  else:
    await setup(name, ctype.parseInt.ConnType, sock)
    result = true

proc tunnel(name, line: string) {.async.} =
  if name in tunnels:
    for client in tunnels[name]:
      if client in connections:
        await connections[client].send(line)

proc availableDevices(name: string) {.async.} =
  if name notin connections:
    echo name, " is not available for label name"
    return
  let
    conn = connections[name]
    devs = if name in tunnels: tunnels[name] else: @[]
  var alldevices = newSeq[string]()
  for c in connections.values:
    if c.ctType == ctSlave:
      alldevices.add(c.label)
  await conn.send("All connection available: " & alldevices.join(" "))
  if devs.len == 0:
    await conn.send("No connected devices available")
  else:
    await conn.send("connected to: " & devs.join(" "))

proc addConnection(name, device: string) {.async.} =
  let conn = connections[name]
  if device notin connections:
    await conn.send("Device to connect not available: " & device)
  else:
    if name notin tunnels:
      tunnels[name] = @[device]
    else:
      var devs = tunnels[name]
      devs.add device
      tunnels[name] = devs

    tunnels[device] = @[name]

    await conn.send("Connected to " & device)
    await connections[device].send("Master connection " & name)


proc removeConnection(name, device: string) {.async.} =
  var
    conn = connections[name]
    devs = tunnels[name]
    pos = devs.find device
    line = ""
  if pos == -1:
    line = "Cannot remove disconnected device"
  else:
    devs.del pos
    tunnels[name] = devs
    line = "Successfully disconnect with " & device
    tunnels.del device

  await conn.send(line)

proc quitConnection(name: string) {.async.} =
  let conn = connections[name]
  conn.sock.close
  connections.del name
  tunnels.del name
  for k, v in tunnels.mpairs:
    if name in v:
      let pos = v.find name
      if pos > -1:
        v.del pos
      tunnels[k] = v

proc parseCommand(line: string): tuple[name, cmd, msg: string] =
  let info = line.split(':', maxsplit = 2)
  if info.len < 3:
    result = ("", "", "")
  else:
    result = (info[0], info[1], info[2])

proc `==`(a, b: Conn): bool =
  a.label == b.label

proc lostConnection(conn: AsyncSocket, line: string): Future[bool]{.async.}=
  result = false
  if line.len == 0 or line == "\c\L":
    var theconn: Conn
    for k, v in connections.mpairs:
      if v.sock == conn:
        theconn = v
        break

    for k, v in tunnels.mpairs:
      if k == theconn.label:
        tunnels.del k
      else:
        var pos = v.find theconn.label
        if pos != -1:
          v.del pos
          tunnels[k] = v
    for k, v in connections.pairs:
      if not (v == theconn) and v.ctType == ctMaster:
        await v.send(theconn.label & " is disconnected")
    theconn.sock.close
    connections.del theconn.label
    result = true

proc handlers(conn: AsyncSocket): Future[bool] {.async.} =
  let line = await conn.recvLine
  echo line
  if await conn.lostConnection(line):
    return false
  let (name, cmd, msg) = line.parseCommand
  result = true
  if name == "" or cmd == "":
    echo "No command available from ", name
    await conn.send("No available command " & cmd & "\c\L")
  else:
    if name notin connections:
      await conn.send(name & " is not available" & "\c\L")
      return true
    elif name != connections[name].label:
      await conn.send(name & " is not registered channel." & "\c\L")
      return true
    case cmd.parseInt.Cmd
    of cmdConnect:
      # use addConnection
      echo "cmdConnect from ", name
      await addConnection(name, msg)
    of cmdSend:
      # use tunnel
      echo "cmdSend from ", name
      await tunnel(name, msg)
    of cmdStatus:
      # use availableDevices
      echo "cmdStatus from ", name
      await availableDevices(name)
    of cmdDisconnect:
      # use removeConnections
      echo "cmdDisconnect from ", name
      await removeConnection(name, msg)
    of cmdQuit:
      echo "cmdQuit from ", name
      await quitConnection(name)
      result = false
    of cmdEchoBack:
      # use echoBack / same as tunnel
      await echoBack(name, msg)
    else:
      echo "No command available from ", name
      await conn.send("No available command " & cmd & "\c\L")

#proc parseCommand(name, line: string) {.async.}

proc processClient(conn: AsyncSocket) {.async.} =
  let successSetup = await setup(conn)
  if not successSetup:
    return
  while true:
    let sessionContinue = await handlers(conn)
    if not sessionContinue:
      break


proc serve() {.async.} =
  connections = newTable[string, Conn]()
  tunnels = newTable[string, seq[string]]()
  var server = newAsyncSocket()
  server.setSockOpt(OptReuseAddr, true)
  server.bindAddr(Port 3000)
  server.listen

  while true:
    let conn = await server.accept()
    asyncCheck processClient(conn)

echo "server is ready"
waitFor serve()
