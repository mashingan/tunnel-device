# Tunnel Device

This implementation works simply tunneling a master connection to devices
connections.  
The implementation depends on various command which provided in ``types.nim``
as ``Cmd`` enum.  
The ``Cmd`` enum defines from the first mentioned from 0 to last feed
string into ``cmdType`` macro.  
This protocol is built above TCP and using format several messaging protocol
formats such:

1. ``connection-type:name-label`` for setup the connection and identification.
2. ``name-label:command-type:command-message`` for doing the operation
based on ``command-type`` given.

## Connection Type

For ``connection-type`` used for setup, there are two types

* ``ctMaster(0)``
* ``ctSlave(1)``

Handling negotiation is done using implemented client during the first
connection for client to server.  
Client with ``ctMaster`` type works as controller for another devices which
have type ``ctSlave``.  
Devices work simply waiting a command from master client and do the command
given.  
At initial implementation, devices will only echoing back the ``command-message``
given to ensure tunneling works ok.  
At later implementation, the tunneling will be done with TLS wrapped TCP
socket for security and various handling necessary as features grow.

## Command-Type

The handler will map the action on what ``command-type`` given from master.  
As mentioned before, ``command-type`` available as ``Cmd`` enum which
defined using macro at compile-time.  
The exact ``command-type`` support can be looked at ``types.nim``, as
current implementation, the supported command are

1. ``cmdConnect(0)``, this command to connect master to device.
2. ``cmdSend(1)``, this command to send the work to device.
3. ``cmdStatus(2)``, echo back the available devices and master connection.
4. ``cmdDisconnect(3)`` to remove master connection with device.
5. ``cmdQuit(4)``, to close the socket and quit client application,
apparently will be only available at master client.
6. ``cmdEchoBack(5)``, early implemented as devices' echo which then to be
sent to master. However since it's implementation will be about same with
``tunnel`` function. The usage of this command is deprecated.
7. Default behaviour which will report back to master that sent command
is unavailable or not supported.

### Protocol example

Protocol examples only have to cover two case, setup protocol and command
protocol.  

#### Setup protocol example

As soon the connection made, this protocol is needed to for setup

```
> 0:master
setup success
```

It will report back if the success complete.


#### Command protocol example

After setup, the recognized protocol is as below

```
> master:2:
All available devices:
No connected devices:
```

As a note for both example, this protocol only needed when one to implement
client, as such, user doesn't need to type all protocol above and only
follow what functionalities client gives.
