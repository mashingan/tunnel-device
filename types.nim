import macros, asyncnet

macro cmdType(command: varargs[string]): untyped =
  result = newTree(nnkEnumTy, newEmptyNode())
  var i = 0
  for cmd in command:
    var tree = newTree(nnkEnumFieldDef, ident($cmd), newIntLitNode(i))
    result.add tree
    inc i

type
  ConnType* = enum
    ctMaster ctSlave

  Conn* = object
    ctType*: ConnType
    sock*: AsyncSocket
    label*: string

  Cmd* = cmdType(
    "cmdConnect",
    "cmdSend",
    "cmdStatus",
    "cmdDisconnect",
    "cmdQuit",
    "cmdEchoBack"
  )
